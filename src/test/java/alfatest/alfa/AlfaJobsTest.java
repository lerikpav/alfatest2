package alfatest.alfa;

import alfatest.AbstractTest;
import alfatest.YandexMainPage;
import org.testng.annotations.Test;

public class AlfaJobsTest extends AbstractTest {

    @Test
    public void alfaJobsTest() {
        YandexMainPage yandexMainPage = new YandexMainPage(driver);
        yandexMainPage.open()
                .search("альфа банк")
                .visitFirstLink();

        AlfaMainPage alfaMainPage = new AlfaMainPage(driver);
        alfaMainPage.toJobs();

        alfaMainPage.checkOnPage("job.alfabank.ru", driver.getCurrentUrl());
    }
}
