package alfatest;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import java.util.concurrent.TimeUnit;

public class AbstractTest {
    protected WebDriver driver;

    @BeforeSuite
    public void getBrowser() {
        WebDriverManager.chromedriver().setup();
        System.setProperty("webdriver.chrome.bin", System.getProperty("webdriver.chrome.driver"));
        ChromeOptions opt = new ChromeOptions();
        opt.addArguments("disable-extensions");
        opt.addArguments("start-maximized");
        driver = new ChromeDriver(opt);
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
    }

    @AfterSuite(alwaysRun = true)
    public void closeBrowser() {
        if (driver != null) {
            driver.quit();
        }
    }
}
