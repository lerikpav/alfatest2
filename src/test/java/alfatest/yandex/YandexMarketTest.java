package alfatest.yandex;

import alfatest.AbstractTest;
import alfatest.YandexMainPage;
import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class YandexMarketTest extends AbstractTest {

    @DataProvider
    private Object[][] products() {
        return new Object[][]{
                {"Мобильные телефоны", "Samsung", 40000, null},
                {"Наушники и Bluetooth-гарнитуры", "Beats", 17000, 25000}
        };
    }

    @Test(dataProvider = "products")
    public void yandexMarketTest(String section, String manufacturer, Integer lowerPrice, Integer higherPrice) {
        YandexMainPage yandexMainPage = new YandexMainPage(driver);
        yandexMainPage.open()
                .toMarket();

        MarketPage market = new MarketPage(driver);
        market.acceptRegion()
                .toElectronicsHover()
                .toSection(section)
                .selectManufacturer(manufacturer)
                .setLowerPrice(lowerPrice)
                .setHigherPrice(higherPrice);

        WebElement productOffer = market.getProductOffers().get(0);
        WebElement productHeader = productOffer.findElement(By.className("n-snippet-cell2__title")).findElement(By.tagName("a"));
        String productName = productHeader.getText();
        productHeader.click();

        ProductPage productPage = new ProductPage(driver);

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(productPage.getProductName())
                .as("Product name mismatch")
                .isEqualTo(productName);
        softly.assertThat(productName)
                .as("Product manufacturer not present in product name")
                .contains(manufacturer);

        softly.assertAll();
    }


}
