package alfatest;

import alfatest.yandex.AbstractYandexPage;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class YandexMainPage extends AbstractYandexPage {

    public YandexMainPage(WebDriver driver) {
        super(driver);
    }

    public YandexMainPage open() {
        String URL = "https://yandex.ru/";
        driver.get(URL);
        return this;
    }

    public YandexMainPage toMarket() {
        WebElement marketLink = driver.findElement(By.xpath("//a[@data-statlog=\"tabs.market\"]"));
        marketLink.click();
        return this;
    }

    public YandexMainPage search(String searchText) {
        WebElement search = driver.findElement(By.id("text"));
        search.sendKeys(searchText);
        search.sendKeys(Keys.ENTER);
        return this;
    }

    public void visitFirstLink() {
        String currentWindow = driver.getWindowHandle();
        WebElement link = driver.findElement(By.cssSelector("div.organic>div>div>a[href$=\"ru/\"]"));
        link.click();
        String newWindow = driver.getWindowHandles().stream().filter(s -> !s.equals(currentWindow)).findFirst().orElse("");
        driver.switchTo().window(newWindow);

    }
}
