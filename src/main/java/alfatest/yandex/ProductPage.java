package alfatest.yandex;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

class ProductPage extends AbstractYandexPage {
    ProductPage(WebDriver driver) {
        super(driver);
    }

    String getProductName() {
        return driver.findElement(By.tagName("h1")).getText();
    }
}
