package alfatest.yandex;

import alfatest.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class AbstractYandexPage extends AbstractPage {

    protected AbstractYandexPage(WebDriver driver) {
        super(driver);
    }

    void waitForLoad() {
        WebElement loader = driver.findElement(By.className("preloadable__preloader_visibility_visible"));
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.invisibilityOf(loader));
    }
}
