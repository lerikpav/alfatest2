package alfatest.yandex;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

@Slf4j
class MarketPage extends AbstractYandexPage {
    MarketPage(WebDriver driver) {
        super(driver);
    }

    MarketPage acceptRegion() {
        try {
            WebElement regionOK = driver.findElement(By.className("n-region-notification__ok"));
            regionOK.click();
        } catch (NoSuchElementException e) {
            log.info("Region already confirmed");
        }
        return this;
    }

    MarketPage toElectronicsHover() {
        WebElement electronics = driver.findElement(By.xpath("//div[contains(@data-bem, \"27903802}\")]"));
        Actions hover = new Actions(driver);
        hover.moveToElement(electronics).perform();
        return this;
    }

    MarketPage toSection(String sectionName) {
        WebElement section = driver.findElement(By.className("n-w-tab__content-container_shown_yes")).findElement(By.xpath(".//a[@title=\"" + sectionName + "\"]"));
        section.click();
        return this;
    }

    MarketPage selectManufacturer(String manufacturer) {
        WebElement checkBox = driver.findElement(By.xpath("//input[@name=\"Производитель " + manufacturer + "\"]"));
        Actions moveToCheckBox = new Actions(driver);
        moveToCheckBox.moveToElement(checkBox)
                .click()
                .perform();
        waitForLoad();
        return this;
    }

    MarketPage setLowerPrice(Integer price) {
        if (price != null) {
            WebElement priceInput = driver.findElement(By.id("glpricefrom"));
            priceInput.sendKeys(price.toString());
            waitForLoad();
        }
        return this;
    }

    MarketPage setHigherPrice(Integer price) {
        if (price != null) {
            WebElement priceInput = driver.findElement(By.id("glpriceto"));
            priceInput.sendKeys(price.toString());
            waitForLoad();
        }
        return this;
    }

    List<WebElement> getProductOffers() {
        return driver.findElements(By.xpath("//div[contains(@data-bem, \"productOffers\")]/div"));
    }
}
