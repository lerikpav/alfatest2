package alfatest.alfa;

import alfatest.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

class AlfaMainPage extends AbstractPage {
    AlfaMainPage(WebDriver driver) {
        super(driver);
    }

    AbstractPage toJobs() {
        WebElement jobs = driver.findElement(By.xpath("//a[@title=\"Вакансии\"]"));
        jobs.click();
        return this;
    }
}
