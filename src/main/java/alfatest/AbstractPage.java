package alfatest;

import org.openqa.selenium.WebDriver;

import static org.assertj.core.api.Assertions.assertThat;

public abstract class AbstractPage {
    protected final WebDriver driver;

    protected AbstractPage(WebDriver driver) {
        this.driver = driver;
    }

    public static void checkOnPage(String pageUrl, String driverPage) {
        assertThat(driverPage)
                .as("Not on expected page")
                .contains(pageUrl);
    }
}
